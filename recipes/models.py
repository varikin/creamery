from django.db import models

class Ingredient(models.Model):
  name = models.CharField(max_length=200, unique=True)
  fat_percentage = models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
  butterfat_percentage = models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
  sugar_pecentage = models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
  grams_per_unit = models.IntegerField(default=1)

  def __str__(self):
    return self.name

class Recipe(models.Model):
  name = models.CharField(max_length=200, unique=True)
  notes = models.TextField()

  def __str__(self):
    return self.name

class RecipeIngredient(models.Model):
  ingredient = models.ForeignKey(Ingredient, on_delete=models.PROTECT)
  recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
  amount = models.DecimalField(max_digits=10, decimal_places=2)

  def __str__(self):
    return self.recipe.name + " " + self.ingredient.name

