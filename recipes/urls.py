from django.urls import path

from . import views

app_name = 'recipes'
urlpatterns = [
  path('', views.RecipeList.as_view(), name='recipe-list'),
  path('<int:pk>/', views.RecipeDetail.as_view(), name='recipe-detail')
]