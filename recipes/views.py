from django.views import generic

from .models import Recipe

class RecipeList(generic.ListView):
  # TODO Paginate
  model = Recipe
  context_object_name = 'recipes'

class RecipeDetail(generic.DetailView):
  model = Recipe
