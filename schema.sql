create table ingredients (
    id integer primary key not null,
    name text not null,
    fat_percentage decimal(4,2) default 0.0,
    butterfat_percentage decimal(4,2) default 0.0,
    sugar_percentage decimal(4, 2) default 0.0
);

create table units (
    id integer primary key not null,
    name text not null
);

create table recipes (
    id integer primary key not null,
    name text not null,
    notes text
);

create table recipe_ingredients (
    id integer primary key not null,
    amount decimal(10,2) not null,
    ingredient_id integer not null references ingredients(id),
    unit_id integer not null references units(id),
    recipe_id integer not null references recipes(id)
);

insert into units (name) values ('g');
insert into ingredients ('name', 'fat_percentage') values ('Califia Original Oatmilk', 3.25),
    ('Refined coconut oil', 100),
    ('Peanut oil', 100),
    ('Egg yolk, large', 26.5);
insert into ingredients ('name', 'sugar_percentage') values ('White sugar', 100);

insert into recipes (name) values ('Oatmilk custard base');

insert into recipe_ingredients (amount, ingredient_id, unit_id, recipe_id) values
(645,
    (select id from ingredients where name='Califia Original Oatmilk'),
    (select id from units where name='g'),
    (select id from recipes where name = 'Oatmilk custard base')
);

insert into recipe_ingredients (amount, ingredient_id, unit_id, recipe_id) values
(90,
    (select id from ingredients where name='Refined coconut oil'),
    (select id from units where name='g'),
    (select id from recipes where name = 'Oatmilk custard base')
);
insert into recipe_ingredients (amount, ingredient_id, unit_id, recipe_id) values 
(40, 
    (select id from ingredients where name='Peanut oil'), 
    (select id from units where name='g'),
    (select id from recipes where name = 'Oatmilk custard base')
);
insert into recipe_ingredients (amount, ingredient_id, unit_id, recipe_id) values 
(75,
    (select id from ingredients where name='Egg yolk, large'), 
    (select id from units where name='g'),
    (select id from recipes where name = 'Oatmilk custard base')
);
insert into recipe_ingredients (amount, ingredient_id, unit_id, recipe_id) values 
(150,
    (select id from ingredients where name='White sugar'), 
    (select id from units where name='g'),
    (select id from recipes where name = 'Oatmilk custard base')
);
